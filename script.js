function Search (querySelectorInput, querySelectorOutput) {
    'use strict';

    //teorijoje rašo, kad geriausia paslėpti viską, ką įmanoma ir palikti tik minimalų interfeisą naudojimuisi
    var _input,
        _output,
        _value;

    function _updateOutput(event) {
        _value = _input.value;
        console.log(_value);
        _output.innerHTML = _value;
    }

    function _init() {
        _input = document.querySelector(querySelectorInput);
        _output = document.querySelector(querySelectorOutput);
        console.log('running init', _input, _output);
        _input.addEventListener('keyup', _updateOutput);
    }

    function init() {
        _init()
    }

    //useriui reikalingas tik šis metodas, kad būtų įgyvendintas funkcionalumas
    return {
        init: init()
    };

}

var search = new Search(".input", ".message");
search.init();